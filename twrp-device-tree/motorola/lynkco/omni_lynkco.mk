#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from lynkco device
$(call inherit-product, device/motorola/lynkco/device.mk)

PRODUCT_DEVICE := lynkco
PRODUCT_NAME := omni_lynkco
PRODUCT_BRAND := motorola
PRODUCT_MODEL := Taro for arm64
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="lynkco-user 12 U3TVS34.1-60-5-5 ad974 release-keys"

BUILD_FINGERPRINT := motorola/lynkco/lynkco:12/U3TVS34.1-60-5-5/ad974:user/release-keys
